const csvToJson = require('csvtojson')
const fs = require('fs')

const matchesFilePath = '../data/matches.csv'
const deliveriesPath = '../data/deliveries.csv'

const getMatchesArray = async () => {
    let matchesArray = await csvToJson().fromFile(matchesFilePath)
    return matchesArray
}

const getDeliveriesArray = async () => {
    let deliveriesArray = await csvToJson().fromFile(deliveriesPath)
    return deliveriesArray
}


module.exports.getMatchesArray = getMatchesArray
module.exports.getDeliveriesArray = getDeliveriesArray