const { getMatchesArray, getDeliveriesArray } = require('./index')
const fs = require('fs')


//------------ problem 1 -----------------

const getMatchesPlayed = async () => {

    const matchesArray = await getMatchesArray()

    const yearMatchesCountObj = matchesArray.reduce((object, current) => {
        object[current.season] = object[current.season] + 1 || 1
        return object
    }, {})
    //console.log(yearMatchesCountObj)


    fs.writeFileSync("../public/output/matchesPerYear.json", JSON.stringify(yearMatchesCountObj), "utf-8", (err) => {
        if (err) {
            console.log(err)
        }
    })

    return yearMatchesCountObj
}

//------------ problem 2 -----------------

const getMatchesWonPerTeam = async () => {

    const matchesArray = await getMatchesArray()

    const teamYearWinsObj = matchesArray.reduce((resultObj, current) => {
        if (resultObj[current.winner]) {
            resultObj[current.winner][current.season] = resultObj[current.winner][current.season] + 1 || 1
        } else {
            resultObj[current.winner] = {}
            resultObj[current.winner][current.season] = 1
        }
        return resultObj
    }, {})

    //console.log(teamYearWinsObj)


    fs.writeFileSync("../public/output/matchesWonPerTeam.json", JSON.stringify(teamYearWinsObj), "utf-8", (err) => {
        if (err) {
            console.log(err)
        }
    })

    return teamYearWinsObj

}
//------------ problem 3 -----------------
const getExtraRunsConcededPerTeam = async (year) => {
    const matchesArray = await getMatchesArray()
    const deliveriesArray = await getDeliveriesArray()

    const matchesIdOfGivenYear = matchesArray.filter((each) => {
        return parseInt(each.season) === year
    }).map((each) => {
        return parseInt(each.id)
    })

    const deliveriesOfGivenYear = deliveriesArray.filter((each) => {
        return matchesIdOfGivenYear.includes(parseInt(each.match_id))
    })

    const deliveriesWithExtras = deliveriesOfGivenYear.filter((each) => {
        return parseInt(each.extra_runs) > 0
    })

    const teamExtrasObj = deliveriesWithExtras.reduce((resultObj, current) => {
        resultObj[current.bowling_team] = resultObj[current.bowling_team] + parseInt(current.extra_runs) || parseInt(current.extra_runs)
        return resultObj
    }, {})

    //console.log(teamExtrasObj)

    fs.writeFileSync("../public/output/extraRunsConcededPerTeam.json", JSON.stringify(teamExtrasObj), "utf-8", (err) => {
        if (err) {
            console.log(err)
        }
    })

    return teamExtrasObj
}


//------------ problem 4 -----------------

const getTopTenEconomcalBowlers = async (year) => {
    const matchesArray = await getMatchesArray()
    const deliveriesArray = await getDeliveriesArray()

    let matchesIdsOfGivenYear = matchesArray.filter((each) => {
        return each.season == year
    }).map((each) => {
        return each.id
    })

    let deliveriesOfGivenYear = deliveriesArray.filter((eachDelivery) => {
        return matchesIdsOfGivenYear.includes(eachDelivery.match_id)
    })

    let bowlerEconomyObj = deliveriesOfGivenYear.reduce((resultObj, current) => {
        if (resultObj[current.bowler]) {
            resultObj[current.bowler]["runs"] += (parseInt(current.total_runs) - parseInt(current.bye_runs) - parseInt(current.legbye_runs))
            resultObj[current.bowler]["ballsCount"] += 1
            resultObj[current.bowler]["economy"] = parseFloat((resultObj[current.bowler]["runs"] / (resultObj[current.bowler]["ballsCount"] / 6)).toFixed(2))

        } else {
            resultObj[current.bowler] = {}
            resultObj[current.bowler]["runs"] = (parseInt(current.total_runs) - parseInt(current.bye_runs) - parseInt(current.legbye_runs))
            resultObj[current.bowler]["ballsCount"] = 1
            resultObj[current.bowler]["economy"] = parseFloat((resultObj[current.bowler]["runs"] / (resultObj[current.bowler]["ballsCount"] / 6)).toFixed(2))

        }

        return resultObj
    }, {})

    //console.log(bowlerEconomyObj)

    const topTenEconomyBowlersArray = Object.entries(bowlerEconomyObj).sort((a, b) => {
        if (a[1]["economy"] > b[1]['economy']) {
            return 1
        } else if (a[1]["economy"] < b[1]['economy']) {
            return -1
        } else {
            return 0
        }
    }).slice(0, 10)

    const topTenEconomyBowlersObj = topTenEconomyBowlersArray.reduce((resultObj, current) => {
        resultObj[current[0]] = current[1]["economy"]
        return resultObj
    }, {})

    //console.log(topTenEconomyBowlersObj)

    fs.writeFileSync("../public/output/topTenEconomyBowlers.json", JSON.stringify(topTenEconomyBowlersObj), "utf-8", (err) => {
        if (err) {
            console.log(err)
        }
    })

    return topTenEconomyBowlersObj
}

//------------ problem 5 ------------------

const getTossAndMatchWins = async () => {

    const matchesArray = await getMatchesArray()


    const teamsWinningTossAndMatchObj = matchesArray.filter((each) => {
        return each.toss_winner === each.winner
    }).reduce((resultObj, current) => {
        resultObj[current.winner] = resultObj[current.winner] + 1 || 1
        return resultObj
    }, {})

    //console.log(teamsWinningTossAndMatchObj)

    fs.writeFileSync("../public/output/tossAndMatchWinsPerTeam.json", JSON.stringify(teamsWinningTossAndMatchObj), "utf-8", (err) => {
        if (err) {
            console.log(err)
        }
    })

    return teamsWinningTossAndMatchObj
}

//----------- problem 6 -------------------

const getplayersWithMaxMOMsPerYear = async () => {

    const matchesArray = await getMatchesArray()

    const yearPlayerMomsObj = matchesArray.reduce((resultObj, current) => {
        if (resultObj[current.season]) {
            resultObj[current.season][current.player_of_match] = resultObj[current.season][current.player_of_match] + 1 || 1

        } else {
            resultObj[current.season] = {}
            resultObj[current.season][current.player_of_match] = 1
        }
        return resultObj
    }, {})

    const yearPlayerMomsArray = Object.entries(yearPlayerMomsObj)

    const yearPlayerWithMaxMomsObj = yearPlayerMomsArray.reduce((resultObj, current) => {

        let playerMomCountArray = Object.entries(current[1]).sort((a, b) => {
            return b[1] - a[1]
        })
        let maxMomCount = playerMomCountArray[0][1]
        let playersWitMaxMoms = playerMomCountArray.filter((each) => {      // works correctly when two batsmen Mom's equal to  maxMomCount
            return each[1] === maxMomCount
        })
        let maxMomPlayer = Object.fromEntries(playersWitMaxMoms)
        resultObj[current[0]] = maxMomPlayer
        return resultObj


    }, {})

    //console.log(yearPlayerWithMaxMomsObj)
    fs.writeFileSync("../public/output/playersWithMaxMOMsPerYear.json", JSON.stringify(yearPlayerWithMaxMomsObj), "utf-8", (err) => {
        if (err) {
            console.log(err)
        }
    })

    return yearPlayerWithMaxMomsObj
}

//----------------- problem 7 ------------------


const getStrikeRateOfBatsmanForEachSeason = async (playerName) => {

    const matchesArray = await getMatchesArray()
    const deliveriesArray = await getDeliveriesArray()

    const ballsFacedByPlayerArray = deliveriesArray.filter((each) => {
        return each.batsman === playerName
    })

    const ballsFacedByPlayerArrayWithSeason = ballsFacedByPlayerArray.map((each) => {
        const year = matchesArray.filter((e) => e.id === each.match_id).map((match) => match.season)
        return { ...each, "season": year[0] }
    })

    const uniqueSeasonsPlayedByPlayer = ballsFacedByPlayerArrayWithSeason.map((each) => {
        return each.season
    }).filter((value, index, arr) => {
        return arr.indexOf(value) == index
    })

    const playerYearStrikeRateObj = uniqueSeasonsPlayedByPlayer.reduce((obj, eachYear) => {

        let ballsFacedInSpecificYear = ballsFacedByPlayerArrayWithSeason.filter((eachBall) => {
            return eachBall.season == eachYear
        })
        let ballsFacedCount = ballsFacedInSpecificYear.length

        let runsScored = ballsFacedInSpecificYear.reduce((acc, curr) => {
            acc = acc + parseInt(curr.batsman_runs)
            return acc
        }, 0)

        let strikeRate = ((runsScored / ballsFacedCount) * 100).toFixed(2)

        obj[eachYear] = strikeRate
        return obj

    }, {})
    //console.log(playerYearStrikeRateObj)

    fs.writeFileSync("../public/output/strikeRateOfPlayerPerSeason.json", JSON.stringify(playerYearStrikeRateObj), "utf-8", (err) => {
        if (err) {
            console.log(err)
        }
    })

    return playerYearStrikeRateObj
}

//------------ problem 8 ---------------------

const getHighestOnePlayerDismissedByOtherPlayer = async () => {

    const deliveriesArray = await getDeliveriesArray()

    const dismissalDeliveriesArray = deliveriesArray.filter(function (each) {
        return each.dismissal_kind != "" && each.dismissal_kind != "run out"
    })

    const bowlerBatsmenPairsArray = dismissalDeliveriesArray.map((each) => {
        return [each.bowler, each.batsman]
    })

    const occurrencesObj = bowlerBatsmenPairsArray.reduce(function (resultObj, current) {
        resultObj[current] = resultObj[current] + 1 || 1
        return resultObj
    }, {});

    const sortedBowlerBatsmenPairCountArray = Object.entries(occurrencesObj).sort((a, b) => b[1] - a[1])

    let maxDismissalCount = sortedBowlerBatsmenPairCountArray[0][1]

    const HighestBowlerBatsmenDismissalPairsArray = sortedBowlerBatsmenPairCountArray.filter((each) => {
        return each[1] == maxDismissalCount
    })

    const HighestPlayerToPlayerDismissalsObj = HighestBowlerBatsmenDismissalPairsArray.reduce((reducingObj, current) => {
        let bowler = current[0].split(",")[0]
        let batsmen = current[0].split(",")[1]
        const tempObj = {}
        tempObj[batsmen] = current[1]
        reducingObj[bowler] = { ...reducingObj[bowler], ...tempObj }

        return reducingObj
    }, {})

    //console.log(HighestPlayerToPlayerDismissalsObj)

    fs.writeFileSync("../public/output/HighestOnePlayerDismissedByOtherPlayer.json", JSON.stringify(HighestPlayerToPlayerDismissalsObj), "utf-8", (err) => {
        if (err) {
            console.log(err)
        }
    })


    return HighestPlayerToPlayerDismissalsObj
}

//------------problem 9 --------------------

const getBowlerWithBestEconomyInSuperOvers = async () => {

    const deliveriesArray = await getDeliveriesArray()

    const superOverDeliveries = deliveriesArray.filter((each) => {
        return each.is_super_over === "1"
    })

    const bowlersEconomyObj = superOverDeliveries.reduce((reducingObj, current) => {
        if (reducingObj[current.bowler]) {
            reducingObj[current.bowler]["runs"] += (parseInt(current.total_runs) - parseInt(current.bye_runs) - parseInt(current.legbye_runs))
            reducingObj[current.bowler]["ballsCount"] += 1
            reducingObj[current.bowler]["economy"] = parseFloat((reducingObj[current.bowler]["runs"] / (reducingObj[current.bowler]["ballsCount"] / 6)).toFixed(2))
        } else {
            reducingObj[current.bowler] = {}
            reducingObj[current.bowler]["runs"] = parseInt(current.total_runs) - parseInt(current.bye_runs) - parseInt(current.legbye_runs)
            reducingObj[current.bowler]["ballsCount"] = 1
            reducingObj[current.bowler]["economy"] = parseFloat((reducingObj[current.bowler]["runs"] / (reducingObj[current.bowler]["ballsCount"] / 6)).toFixed(2))
        }
        return reducingObj
    }, {})


    const bowlersEconomyInSOversArray = Object.entries(bowlersEconomyObj).sort((a, b) => {
        if (a[1]["economy"] > b[1]['economy']) {
            return 1
        } else if (a[1]["economy"] < b[1]['economy']) {
            return -1
        } else {
            return 0
        }
    })

    let bestEconomyInSOvers = bowlersEconomyInSOversArray[0][1]["economy"]

    let bestEconomicalBowlersInSoArray = bowlersEconomyInSOversArray.filter((each) => {
        return each[1]["economy"] == bestEconomyInSOvers
    })

    let bestEconomicalBowlerInSoObj = bestEconomicalBowlersInSoArray.reduce((reducingObj, current) => {    // works correctly when two bowlers have best economy value
        reducingObj[current[0]] = current[1]["economy"]
        return reducingObj
    }, {})

    //console.log(bestEconomicalBowlerInSoObj)

    fs.writeFileSync("../public/output/bestEconomyBowlerInSOvers.json", JSON.stringify(bestEconomicalBowlerInSoObj), "utf-8", (err) => {
        if (err) {
            console.log(err)
        }
    })

    return bestEconomicalBowlerInSoObj
}




getMatchesPlayed()
getMatchesWonPerTeam()
getExtraRunsConcededPerTeam(2016)
getTopTenEconomcalBowlers(2015)
getTossAndMatchWins()
getplayersWithMaxMOMsPerYear()
getStrikeRateOfBatsmanForEachSeason("V Kohli");
getBowlerWithBestEconomyInSuperOvers()
getBowlerWithBestEconomyInSuperOvers()
getHighestOnePlayerDismissedByOtherPlayer()